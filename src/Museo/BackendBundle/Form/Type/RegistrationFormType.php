<?php

namespace Museo\BackendBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;
use FOS\UserBundle\Form\Type\RegistrationFormType as BaseType;

class RegistrationFormType extends BaseType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
    	// add your custom field
    	$builder->add('name');
    	$builder->add('last_name');
    	$builder->add('role', 'choice', array(
    	            'choices'  => array(
    	                'role_admin' => 'Admin',
    	                'role_super_admin'     => 'Super Admin'
    				),
	   	));
    	
        parent::buildForm($builder, $options);

        
    }

    public function getName()
    {
        return 'backend_user_registration';
    }
}
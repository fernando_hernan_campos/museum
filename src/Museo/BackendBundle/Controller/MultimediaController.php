<?php

namespace Museo\BackendBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Museo\BackendBundle\Entity\Multimedia;
use Museo\BackendBundle\Form\MultimediaType;
use Symfony\Component\HttpFoundation\Response;

/**
 * Multimedia controller.
 *
 * @Route("/backend/multimedia")
 */
class MultimediaController extends Controller
{

    /**
     * Lists all Multimedia entities.
     *
     * @Route("/", name="backend_multimedia")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Request $request)
    {


        $element_id = $request->get('element_id');
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MuseoBackendBundle:Multimedia')->findBy(
               array('elemento_id' => $element_id));

        return array(
            'entities' => $entities,
            'element_id' => $element_id
        );
    }
    /**
     * Creates a new Multimedia entity.
     *
     * @Route("/", name="backend_multimedia_create")
     * @Method("POST")
     */
    public function createAction(Request $request)
    {
        $entity  = new Multimedia();
        $form = $this->createForm(new MultimediaType(), $entity);
        $form->bind($request);

        //Set Tipo de multimedia
        $file_name = $entity->getFileName();
        $extension = substr($file_name,(strpos($file_name, '.') + 1 ));
        $entity->getTypeExtension($extension);

        //Set  el path del archivo
        $em = $this->getDoctrine()->getManager();
        $element_entity = $em->getRepository('MuseoBackendBundle:Element')->find($entity->getElementoId());
        $entity->setPath($element_entity->getZone()->getSala()->getImagesFolderRelative());

        //hardcode del idioma (deberia tomarse del request, poniendo una seleccion del idioma cuando el archivo es de tipo audio o video)
        $entity->setIdioma('');
        $entity->setElemento($element_entity);

        $em = $this->getDoctrine()->getManager();
        $em->persist($entity);
        $em->flush();

       return $this->redirect($this->generateUrl('backend_multimedia', array('element_id' => $entity->getElementoId())));
       // }else{
        //    die("formulario invalido");
       // }
    }

    /**
     * Displays a form to create a new Multimedia entity.
     *
     * @Route("/new", name="backend_multimedia_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction(Request $request)
    {
        $entity = new Multimedia();
        $form   = $this->createForm(new MultimediaType(), $entity);

        $element_id = $request->get('element_id');

        $em = $this->getDoctrine()->getManager();
        $element_entity = $em->getRepository('MuseoBackendBundle:Element')->find($element_id);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'element_id' => $element_id,
            'sala_name' => $element_entity->getZone()->getSala()->getShortname()
        );
    }

    /**
     * Finds and displays a Multimedia entity.
     *
     * @Route("/{id}", name="backend_multimedia_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MuseoBackendBundle:Multimedia')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Multimedia entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Multimedia entity.
     *
     * @Route("/{id}/edit", name="backend_multimedia_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MuseoBackendBundle:Multimedia')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Multimedia entity.');
        }

        $editForm = $this->createForm(new MultimediaType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Edits an existing Multimedia entity.
     *
     * @Route("/{id}", name="backend_multimedia_update")
     * @Method("PUT")
     * @Template("MuseoBackendBundle:Multimedia:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MuseoBackendBundle:Multimedia')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Multimedia entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new MultimediaType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('backend_multimedia_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Multimedia entity.
     *
     * @Route("/{id}", name="backend_multimedia_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('MuseoBackendBundle:Multimedia')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Multimedia entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('backend_multimedia'));
    }

    /**
     * Creates a form to delete a Multimedia entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }

     /**
     * Upload a multimedia.
     *
     * @Route("/{element_id}/upload", name="backend_multimedia_upload")
     */
    public function uploadAction($element_id)
    {
        //| $content = "There was an error uploading the file, please try again!";
        
        file_put_contents(__DIR__.'/../../../../web/bundles/museo/images/salas/log.txt', "PRUEBAAAAAsasswdasdasd");


        $em = $this->getDoctrine()->getManager();
        $element = $em->getRepository('MuseoBackendBundle:Element')->find($element_id);
        

        $target_path = $element->getZone()->getSala()->getImagesFolder();
    

        //  $content = "There was an error uploading the file, please try again!";
         //  file_put_contents(__DIR__.'/../../../../web/bundles/museo/images/salas/log.txt', "PRUEBAAAAA");
         //  die();
        $target_path = $target_path . '/' . basename( $_FILES['Filedata']['name']);

       //$target_path = $sala->getImagesFolder() . basename( $_FILES['Filedata']['name']); 

       if(move_uploaded_file($_FILES['Filedata']['tmp_name'], $target_path)) {
            $content = "The file ".  basename( $_FILES['Filedata']['name']). " has been uploaded";
            file_put_contents(__DIR__.'/../../../../web/bundles/museo/images/salas/log.txt', $content);
       } else{
            $content = "There was an error uploading the file, please try again!";
            file_put_contents(__DIR__.'/../../../../web/bundles/museo/images/salas/log.txt', $content);
       }

       return new Response('');
    }


}

<?php

namespace Museo\BackendBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Museo\BackendBundle\Entity\Sala;
use Museo\BackendBundle\Form\SalaType;
use Symfony\Component\HttpFoundation\Response;

/**
 * Sala controller.
 *
 * @Route("/backend/sala")
 */
class SalaController extends Controller
{

    /**
     * Lists all Sala entities.
     *
     * @Route("/", name="backend_sala")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MuseoBackendBundle:Sala')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Sala entity.
     *
     * @Route("/", name="backend_sala_create")
     * @Method("POST")
     * @Template("MuseoBackendBundle:Sala:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity  = new Sala();
        $form = $this->createForm(new SalaType(), $entity);
        $form->bind($request);

        $file_es = $request->get('audio_es');
        $file_en = $request->get('audio_en');

        if ($form->isValid()) {

             //creacion de las carpetas para los archivos multimedia de las salas.
            mkdir($entity->getImagesFolder());

            //Copiar los archivos de audio a la carpeta de la sala
            rename($entity->getImagesFolder().'/../temp/'.$file_es,$entity->getImagesFolder().'/audio_es.mp3');
            rename($entity->getImagesFolder().'/../temp/'.$file_en,$entity->getImagesFolder().'/audio_en.mp3');


            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();



            return $this->redirect($this->generateUrl('backend_sala_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Displays a form to create a new Sala entity.
     *
     * @Route("/new", name="backend_sala_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Sala();
        $form   = $this->createForm(new SalaType(), $entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Sala entity.
     *
     * @Route("/{id}", name="backend_sala_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MuseoBackendBundle:Sala')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Sala entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Sala entity.
     *
     * @Route("/{id}/edit", name="backend_sala_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MuseoBackendBundle:Sala')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Sala entity.');
        }

        $editForm = $this->createForm(new SalaType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Edits an existing Sala entity.
     *
     * @Route("/{id}", name="backend_sala_update")
     * @Method("PUT")
     * @Template("MuseoBackendBundle:Sala:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MuseoBackendBundle:Sala')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Sala entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new SalaType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('backend_sala_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Sala entity.
     *
     * @Route("/{id}", name="backend_sala_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('MuseoBackendBundle:Sala')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Sala entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('backend_sala'));
    }

    /**
     * Creates a form to delete a Sala entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }

    /**
     * Upload a audio.
     *
     * @Route("/upload", name="backend_sala_upload_audio")
     */
    public function uploadAudioAction(){

        file_put_contents(__DIR__.'/../../../../web/bundles/museo/images/salas/log.txt', var_dump($_FILES,true));
        $target_path = __DIR__.'/../../../../web/bundles/museo/images/salas/temp';

        $target_path = $target_path . '/' . basename( $_FILES['Filedata']['name']);
        if(move_uploaded_file($_FILES['Filedata']['tmp_name'], $target_path)) {
           // $content = "The file ".  basename( $_FILES['Filedata']['name']). " has been uploaded";
           // file_put_contents(__DIR__.'/../../../../web/bundles/museo/images/salas/log.txt', $content);
        } else{
          //  $content = "There was an error uploading the file, please try again!";
           // file_put_contents(__DIR__.'/../../../../web/bundles/museo/images/salas/log.txt', $content);
        }

        return new Response('');
    }
}

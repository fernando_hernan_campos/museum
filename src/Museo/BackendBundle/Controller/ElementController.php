<?php

namespace Museo\BackendBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Museo\BackendBundle\Entity\Element;
use Museo\BackendBundle\Form\ElementType;

/**
 * Element controller.
 *
 * @Route("/backend/element")
 */
class ElementController extends Controller
{

    /**
     * Lists all Element entities.
     *
     * @Route("/", name="backend_element")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MuseoBackendBundle:Element')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Element entity.
     *
     * @Route("/", name="backend_element_create")
     * @Method("POST")
     * @Template("MuseoBackendBundle:Element:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity  = new Element();
        $form = $this->createForm(new ElementType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('backend_element_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Displays a form to create a new Element entity.
     *
     * @Route("/new", name="backend_element_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Element();
        $form   = $this->createForm(new ElementType(), $entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Element entity.
     *
     * @Route("/{id}", name="backend_element_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MuseoBackendBundle:Element')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Element entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView()
        );
    }

    /**
     * Displays a form to edit an existing Element entity.
     *
     * @Route("/{id}/edit", name="backend_element_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MuseoBackendBundle:Element')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Element entity.');
        }

        $editForm = $this->createForm(new ElementType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Edits an existing Element entity.
     *
     * @Route("/{id}", name="backend_element_update")
     * @Method("PUT")
     * @Template("MuseoBackendBundle:Element:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MuseoBackendBundle:Element')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Element entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new ElementType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('backend_element_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Element entity.
     *
     * @Route("/{id}", name="backend_element_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('MuseoBackendBundle:Element')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Element entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('backend_element'));
    }

    /**
     * Creates a form to delete a Element entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}

<?php
// src/Acme/UserBundle/Entity/User.php

namespace Museo\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="elemento")
 */
class Element
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
	/**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255, nullable=false)
     * @Assert\NotBlank(message="Please enter a name to sala.", groups={"Registration", "Profile"})
     * 
     */
    private $name;
    
    /**
    * @var string
    *
    * @ORM\Column(name="descripcion", type="text", nullable=false)
    * @Assert\NotBlank(message="Please enter a description to sala.", groups={"Registration", "Profile"})
    * 
    */
    private $description;

    /**
    * @var string
    *
    * @ORM\Column(name="image", type="text", type="string", length=255, nullable=true)
    * 
    */
    private $image;

    /**
    * @var Zone
    *
    * @ORM\ManyToOne(targetEntity="Zone", inversedBy="elements")
    * 
    */
    public $zone;

    /**
    * @var ArrayCollection
    *
    * @ORM\OneToMany(targetEntity="Multimedia", mappedBy="elemento")
    *
    */
    public $multimedias;
    
    
    public function getId(){
        return $this->id;
    }
    
    public function getName(){
    	return $this->name;
    }
    
    public function setName($name){
    	$this->name = $name;
    }

    public function setDescription($description)
    {
        $this->description = $description;        
    }

    public function getDescription()
    {
        return $this->description;
    }
    
    public function setZone($zone)
    {
        $this->zone = $zone;
    }

    public function getZone()
    {
        return $this->zone;
    }

    public function __toString()
    {
        return (string) $this->getName();
    }

    public function setImage($image)
    {
        $this->image = $image;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function getMultimedias()
    {
        return $this->multimedias;
    }

    public function setMultimedias($multimedias)
    {
        $this->multimedias = $multimedias;
    }
}
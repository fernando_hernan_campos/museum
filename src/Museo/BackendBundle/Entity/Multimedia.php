<?php
// src/Acme/UserBundle/Entity/User.php

namespace Museo\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="multimedia")
 */
class Multimedia
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id_multimedia",type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
	/**
     * @var string
     *
     * @ORM\Column(name="file_name", type="string", length=128, nullable=false)
     * 
     */
    private $fileName;
    
    /**
    * @var string
    *
    * @ORM\Column(name="descripcion", type="text", nullable=false)
    * @Assert\NotBlank(message="Please enter a description to the multimedia.", groups={"Registration", "Profile"})
    * 
    */
    private $description;

    /**
    * @var string
    *
    * @ORM\Column(name="path", type="string", length=256, nullable=false)
    * 
    */
    private $path;

    /**
    * @var char
    *
    * @ORM\Column(name="tipo",  type="string", length=1, nullable=false)
    * 
    */
    private $tipo;

    /**
    * @var string
    *
    * @ORM\Column(name="idioma", type="string", length=20, nullable=true)
    * 
    */
    private $idioma;

    
    /**
    * @var integer
    *
    * @ORM\Column(name="elemento_id", type="integer")
    * 
    */
    private $elemento_id;

    /**
    * @var Elemento
    *
    * @ORM\ManyToOne(targetEntity="Element", inversedBy="multimedias")
    * 
    */
    public $elemento;
    
    
    public function getId(){
        return $this->id;
    }
    
    public function getFileName(){
    	return $this->fileName;
    }
    
    public function setFileName($name){
    	$this->fileName = $name;
    }

    public function setDescription($description)
    {
        $this->description = $description;        
    }

    public function getDescription()
    {
        return $this->description;
    }
    
    public function setPath($path)
    {
        $this->path = $path;
    }

    public function getPath()
    {
        return $this->path;
    }

    public function setTipo($tipo)
    {
        $this->tipo = $tipo;
    }

    public function getTipo()
    {
        return $this->tipo;
    }

    public function setIdioma($idioma)
    {
        $this->idioma = $idioma;
    }

    public function getIdioma()
    {
        return $this->idioma;
    }

    public function __toString()
    {
        return (string) $this->getFileName();
    }

    public function getElemento()
    {
        return $this->elemento;
    }

    public function setElemento($elemento)
    {
        $this->elemento = $elemento;
    }

    public function getElementoId()
    {
        return $this->elemento_id;
    }

    public function setElementoId($element_id)
    {
        $this->elemento_id = $element_id;
    }

    public function getTypeExtension($extension){

        switch (strtolower($extension)) {
            case 'jpg':
            case 'jpeg':
                $this->setTipo('I');
                break;
            case 'mp3':
                $this->setTipo('A');
                break;
            case 'mp4':
                $this->setTipo('V');
                break;
            default:
                break;
        }
    }

    public function getMultimediaRow(){
          switch ($this->getTipo()) {
            case 'I':
                $html = '<img src="{src}" width="100" height="50"></img>';
                break;
            case 'A':
                $html = '<audio src="{src}" controls>Your browser does not support the audio element.</audio>';
                break;
            case 'V':
                $html = '<video src="{src}" controls>Your browser does not support the audio element.</video>';
                break;
            default:
                $html = '';
                break;
        }
        $src =  $this->getPath().'/'.$this->getFileName();

        return str_replace('{src}',$src,$html);
    }
}
<?php
// src/Acme/UserBundle/Entity/User.php

namespace Museo\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="zona")
 */
class Zone
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
	/**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255, nullable=false)
     * @Assert\NotBlank(message="Please enter a name to zone.", groups={"Registration", "Profile"})
     * 
     */
    private $name;
    
    /**
    * @var string
    *
    * @ORM\Column(name="descripcion", type="text", nullable=false)
    * @Assert\NotBlank(message="Please enter a description to the zone.", groups={"Registration", "Profile"})
    * 
    */
    private $description;

    /**
    * @var Sala
    *
    * @ORM\ManyToOne(targetEntity="Sala", inversedBy="zones")
    * 
    */
    public $sala;

    /**
    * @var ArrayCollection
    *
    * @ORM\OneToMany(targetEntity="Element", mappedBy="zone")
    *
    */
    public $elements;

    public function getId(){
        return $this->id;
    }
    
    public function getName(){
    	return $this->name;
    }
    
    public function setName($name){
    	$this->name = $name;
    }

    public function setDescription($description)
    {
        $this->description = $description;        
    }

    public function getDescription()
    {
        return $this->description;
    }
    
    public function setSala($sala)
    {
        $this->sala = $sala;
    }

    public function getSala()
    {
        return $this->sala;
    }

    public function getElements()
    {
        return $this->elements;
    }

    public function setElements($elements)
    {
        $this->elements = $elements;
    }

    public function __toString()
    {
        return (string) $this->getName();
    }

}
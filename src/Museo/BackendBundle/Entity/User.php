<?php
// src/Acme/UserBundle/Entity/User.php

namespace Museo\BackendBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="usuario")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
	/**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     * @Assert\NotBlank(message="Please enter your name.", groups={"Registration", "Profile"})
     * 
     */
    private $name;
    
    /**
    * @var string
    *
    * @ORM\Column(name="last_name", type="string", length=255, nullable=true)
    * @Assert\NotBlank(message="Please enter your last name.", groups={"Registration", "Profile"})
    * 
    */
    private $last_name;
    
    
    /**
    * @var string
    */
    private $role;


    /**
     * @var Sala
     * 
     * @ORM\OneToMany(targetEntity="Sala",mappedBy="user")
     * 
     */
    public $salas;
    
    public function __construct()
    {
        $this->salas = new ArrayCollection();
        parent::__construct();
        // your own logic
    }
    
    public function getName(){
    	return $this->name;
    }
    
    public function setName($name){
    	$this->name = $name;
    }
    
    public function getLastName(){
    	return $this->last_name;
    }
    
    public function setLastName($last_name){
    	$this->last_name = $last_name;
    }
    
    public function setRole($role)
    {
    	$this->roles = array();
    	$this->addRole($role);
    }
    
    public function getRole()
    {
    	$roles = $this->getRoles();
    	return (empty($roles))?'':$roles[0];
    }

    public function setSalas($salas)
    {
        $this->salas = $salas;
    }

    public function getSalas()
    {
        return $this->salas;
    }

    public function __toString()
    {
        return (string) $this->getLastName().' '.$this->getName();
    }
}
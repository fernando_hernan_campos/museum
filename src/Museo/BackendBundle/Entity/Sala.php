<?php
// src/Acme/UserBundle/Entity/User.php

namespace Museo\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="sala")
 */
class Sala
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
	/**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255, nullable=false)
     * @Assert\NotBlank(message="Please enter a name to sala.", groups={"Registration", "Profile"})
     * 
     */
    private $name;
    
    /**
    * @var string
    *
    * @ORM\Column(name="descripcion", type="text", nullable=false)
    * @Assert\NotBlank(message="Please enter a description to sala.", groups={"Registration", "Profile"})
    * 
    */
    private $description;

    /**
    * @var User
    *
    * @ORM\ManyToOne(targetEntity="User", inversedBy="salas")
    * 
    */
    public $user;
    
    /**
    * @var ArrayCollection
    *
    * @ORM\OneToMany(targetEntity="Zone", mappedBy="sala")
    *
    */
    public $zones;
    
    public function getId(){
        return $this->id;
    }
    
    public function getName(){
    	return $this->name;
    }
    
    public function setName($name){
    	$this->name = $name;
    }

    public function setDescription($description)
    {
        $this->description = $description;        
    }

    public function getDescription()
    {
        return $this->description;
    }
    
    public function setUser($user)
    {
        $this->user = $user;
    }

    public function getUser()
    {
        return $this->user;
    }
    
    public function setZones($zones)
    {
    	$this->zones = $zones;
    }
    
    public function getZones()
    {
    	return $this->zones;
    }

    public function __toString()
    {
        return (string) $this->getName();
    }

    public function getImagesFolder()
    {
        return __DIR__.'/../../../../web/bundles/museo/images/salas/'.$this->getShortname();
    }

    public function getImagesFolderRelative()
    {
        return '/bundles/museo/images/salas/'.$this->getShortname();
    }

    public function getShortname()
    {
        return strtolower(str_replace(' ', '_', $this->getName()));
    }
}
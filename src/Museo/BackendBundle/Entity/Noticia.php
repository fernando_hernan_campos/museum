<?php
// src/Acme/UserBundle/Entity/User.php

namespace Museo\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="noticia")
 */
class Noticia
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
	/**
     * @var string
     *
     * @ORM\Column(name="titulo", type="string", length=255, nullable=false)
     * @Assert\NotBlank(message="Please enter the tittle of each notice.", groups={"Registration", "Profile"})
     * 
     */
    private $titulo;
    
    /**
    * @var string
    *
    * @ORM\Column(name="texto", type="text", nullable=false)
    * @Assert\NotBlank(message="Please enter a text for this notice", groups={"Registration", "Profile"})
    * 
    */
    private $texto;

     /**
    * @var string
    *
    * @ORM\Column(name="fecha", type="date", nullable=false)
    * @Assert\NotBlank(message="Please enter a date", groups={"Registration", "Profile"})
    * 
    */
    private $fecha;


    
    public function getId(){
        return $this->id;
    }

    
    public function getTitulo(){
    	return $this->titulo;
    }
    
    public function setTitulo($titulo){
    	$this->titulo = $titulo;
    }

    public function setTexto($texto)
    {
        $this->texto = $texto;        
    }

    public function getTexto()
    {
        return $this->texto;
    }
    
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    }
    
    public function getFecha()
    {
       return $this->fecha;
    }
    
      public function __toString()
    {
        return (string) $this->getTitulo();
    }

}
<?php

namespace Museo\FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Museo\FrontendBundle\Entity\Mp3Data;

/**
 * Element controller.
 *
 * @Route("/")
 */
class DefaultController extends Controller
{
    /**
     * @Route("/")
     * @Template()
     */
    public function indexAction()
    {
        return $this->render('MuseoFrontendBundle:Default:index.html.twig');
    }

    /**
     * @Route("/about", name="frontend_about")
     * @Template()
     */
    public function aboutAction(){
        return $this->render('MuseoFrontendBundle:Default:about.html.twig');
    }
    
    
     /**
     * @Route("/home", name="frontend_Home")
     * @Template()
     */
    public function homeAction(){
        return $this->render('MuseoFrontendBundle:Default:index.html.twig');
    }
    
    public function articulosAction()
    {
        //-- Simulamos obtener los datos de la base de datos cargando los art�culos a un array
        $articulos = array(
            array('id' => 1, 'title' => 'Articulo numero 1', 'created' => '2011-01-01'),
            array('id' => 2, 'title' => 'Articulo numero 2', 'created' => '2011-01-01'),
            array('id' => 3, 'title' => 'Articulo numero 3', 'created' => '2011-01-01'),
        );
        return $this->render('MuseoFrontendBundle:Default:articulos.html.twig', array('articulos' => $articulos));
    }
    
    
    /**
     * @Route("/paseoVirtual", name="frontend_PaseoVirtual")
     * @Template()
     */
    
    public function paseoVirtualAction(){
    
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MuseoBackendBundle:Sala')->findAll();

        return array(
            'entities' => $entities,
        );
       //return $this->render('MuseoFrontendBundle:Default:paseoVirtual.html.twig');
    
    }
	
	/**
     * @Route("/paseoVirtualView", name="frontend_PaseoVirtualView")
     * @Method("POST")
     * @Template()
     */	
	public function paseoVirtualViewAction(Request $request)
	{
		$em = $this->getDoctrine()->getManager();

        $salas = explode(",", $request->get('salas'));
        $languaje = $request->get('languaje');

        $entities = $em->getRepository('MuseoBackendBundle:Sala')->findAll();

        $entities_ordened = array();

		//obtener para cada uno de las salas, las imagenes y el archivo de audio.
		foreach ($entities as $sala) {

            if(!in_array($sala->getId(),$salas))
                continue;

			$images_path = $sala->getImagesFolder();
			$images_url_path = $sala->getImagesFolderRelative();
			
			$file = $images_path.'/audio_'.$languaje.'.mp3';
			if(file_exists($file)){
				$model = new Mp3Data($file);
				$audio_data = $model->get_metadata();
			}else{
				$audio_data = array();
			}
			
			$files[$sala->getName()] = array('path'=>$images_path,
											'url_path'=> $images_url_path, 
											'audio'=>'audio_'.$languaje.'.mp3',
											'audio_data' => $audio_data,
											'images'=>array());
			
			$dh  = opendir($images_path);
			while (false !== ($filename = readdir($dh))) {
				//Solo almaceno los archivos que sean imagenes
				if(preg_match('([^\s]+(\.(?i)(jpg|jpeg))$)', $filename, $matches)){
					$files[$sala->getName()]['images'][] = $filename;
				}
			}

            //armo el arreglo ordenado segun el criterio elegido por el usuario
            $entities_ordened[array_search($sala->getId(),$salas)] = $sala;

            // die(var_dump($entities_ordened));

		}
		ksort($entities_ordened);

        //die(var_dump($entities_ordened));
		return array(
            'entities' => $entities_ordened,
            'files'=>$files,
        );
	}
    
    /**
     * @Route("/mailcontact", name="frontend_mailcontact")
     * @Template()
     */
    
    public function mailcontactAction(){
    
      return $this->render('MuseoFrontendBundle:Default:mailcontact.html.twig');
    
    }
    
    
     /**
     * @Route("/comollegar", name="frontend_comollegar")
     * @Template()
     */
    
    public function comollegarAction(){
    
      return $this->render('MuseoFrontendBundle:Default:comollegar.html.twig');
    
    }
    
    
     
     /**
     * @Route("/exposiciones", name="frontend_exposiciones")
     * @Template()
     */
    
    public function exposicionesAction(){
    
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MuseoBackendBundle:Sala')->findAll();

        return array(
            'entities' => $entities,
        );    
    }
    
   
    
      /**
     * @Route("/sala_details", name="frontend_sala_details")
     * @Template("MuseoFrontendBundle:Default:saladetails.html.twig")
     */
    
    public function sala_detailsAction(Request $request){
    
      
        $sala_id = $request->get('id');
        $em = $this->getDoctrine()->getManager();

        $sala = $em->getRepository('MuseoBackendBundle:Sala')->find($sala_id);
         
        return array(
            'entities' => $sala->getZones(),
           // 'element_id' => $element_id
        );
        
      //return $this->render('MuseoFrontendBundle:Default:saladetails.html.twig');
    
    }
    
      /**
     * @Route("/element_details", name="frontend_element_details")
     * @Template("MuseoFrontendBundle:Default:elementDetails.html.twig")
     */
    
    public function elementDetailsAction(Request $request){
    
      
        $element_id = $request->get('element_id');
        $em = $this->getDoctrine()->getManager();

        $elemento = $em->getRepository('MuseoBackendBundle:Element')->find($element_id);
         
        return array(
            'entity' => $elemento,
           // 'element_id' => $element_id
        );
        
      //return $this->render('MuseoFrontendBundle:Default:saladetails.html.twig');
    
    }
    
    
    
    
    
    
}




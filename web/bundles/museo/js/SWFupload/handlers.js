/* Demo Note:  This demo uses a FileProgress class that handles the UI for displaying the file name and percent complete.
The FileProgress class is not part of SWFUpload.
*/
/********************
   Event Handlers
   These are my custom event handlers to make my
   web application behave the way I went when SWFUpload
   completes different tasks.  These aren't part of the SWFUpload
   package.  They are part of my application.  Without these none
   of the actions SWFUpload makes will show up in my application.
   ********************** */
function fileQueued(file) {
	try {
console.log(this.customSettings.progressTarget);
		var progress = new FileProgress(file, this.customSettings.progressTarget);

		console.log(this.customSettings.progressTarget);
		progress.setStatus("Pending...");
		progress.toggleCancel(true, this);

	} catch (ex) {
		this.debug(ex);
	}

}

function fileQueueError(file, errorCode, message) {
	try {
		if (errorCode === SWFUpload.QUEUE_ERROR.QUEUE_LIMIT_EXCEEDED) {
			alert("You have attempted to queue too many files.\n" + (message === 0 ? "You have reached the upload limit." : "You may select " + (message > 1 ? "up to " + message + " files." : "one file.")));
			return;
		}

		var progress = new FileProgress(file, this.customSettings.progressTarget);
		progress.setError();
		progress.toggleCancel(false);

		switch (errorCode) {
		case SWFUpload.QUEUE_ERROR.FILE_EXCEEDS_SIZE_LIMIT:
			progress.setStatus("File is too big.");
			this.debug("Error Code: File too big, File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
			break;
		case SWFUpload.QUEUE_ERROR.ZERO_BYTE_FILE:
			progress.setStatus("Cannot upload Zero Byte files.");
			this.debug("Error Code: Zero byte file, File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
			break;
		case SWFUpload.QUEUE_ERROR.INVALID_FILETYPE:
			progress.setStatus("Invalid File Type.");
			this.debug("Error Code: Invalid File Type, File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
			break;
		default:
			if (file !== null) {
				progress.setStatus("Unhandled Error");
			}
			this.debug("Error Code: " + errorCode + ", File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
			break;
		}
	} catch (ex) {
        this.debug(ex);
    }
}

function fileDialogComplete(numFilesSelected, numFilesQueued) {
	try {
		if (numFilesSelected > 0) {
			//document.getElementById(this.customSettings.cancelButtonId).disabled = false;
		}
		
		/* I want auto start the upload and I can do that here */
		this.startUpload();
	} catch (ex)  {
        this.debug(ex);
	}
}

function uploadStart(file) {
	try {
		/* I don't want to do any file validation or anything,  I'll just update the UI and
		return true to indicate that the upload should start.
		It's important to update the UI here because in Linux no uploadProgress events are called. The best
		we can do is say we are uploading.
		 */
		var progress = new FileProgress(file, this.customSettings.progressTarget);
		progress.setStatus("Uploading...");
		progress.toggleCancel(true, this);
	}
	catch (ex) {}
	
	return true;
}

function uploadProgress(file, bytesLoaded, bytesTotal) {
	try {
		var percent = Math.ceil((bytesLoaded / bytesTotal) * 100);

		var progress = new FileProgress(file, this.customSettings.progressTarget);
		progress.setProgress(percent);
		progress.setStatus("Uploading...");
	} catch (ex) {
		this.debug(ex);
	}
}

function uploadSuccess(file, serverData) {
	try {
		var progress = new FileProgress(file, this.customSettings.progressTarget);
		progress.setComplete();
		progress.setStatus("Complete.");
      
		progress.toggleCancel(false);

	} catch (ex) {
		this.debug(ex);
	}
}

function uploadError(file, errorCode, message) {
	try {
		var progress = new FileProgress(file, this.customSettings.progressTarget);
		progress.setError();
		progress.toggleCancel(false);

		switch (errorCode) {
		case SWFUpload.UPLOAD_ERROR.HTTP_ERROR:
			progress.setStatus("Upload Error: " + message);
			this.debug("Error Code: HTTP Error, File name: " + file.name + ", Message: " + message);
			break;
		case SWFUpload.UPLOAD_ERROR.UPLOAD_FAILED:
			progress.setStatus("Upload Failed.");
			this.debug("Error Code: Upload Failed, File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
			break;
		case SWFUpload.UPLOAD_ERROR.IO_ERROR:
			progress.setStatus("Server (IO) Error");
			this.debug("Error Code: IO Error, File name: " + file.name + ", Message: " + message);
			break;
		case SWFUpload.UPLOAD_ERROR.SECURITY_ERROR:
			progress.setStatus("Security Error");
			this.debug("Error Code: Security Error, File name: " + file.name + ", Message: " + message);
			break;
		case SWFUpload.UPLOAD_ERROR.UPLOAD_LIMIT_EXCEEDED:
			progress.setStatus("Upload limit exceeded.");
			this.debug("Error Code: Upload Limit Exceeded, File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
			break;
		case SWFUpload.UPLOAD_ERROR.FILE_VALIDATION_FAILED:
			progress.setStatus("Failed Validation.  Upload skipped.");
			this.debug("Error Code: File Validation Failed, File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
			break;
		case SWFUpload.UPLOAD_ERROR.FILE_CANCELLED:
			// If there aren't any files left (they were all cancelled) disable the cancel button
			if (this.getStats().files_queued === 0) {
				//document.getElementById(this.customSettings.cancelButtonId).disabled = true;
			}
			progress.setStatus("Cancelled");
			progress.setCancelled();
			break;
		case SWFUpload.UPLOAD_ERROR.UPLOAD_STOPPED:
			progress.setStatus("Stopped");
			break;
		default:
			progress.setStatus("Unhandled Error: " + errorCode);
			this.debug("Error Code: " + errorCode + ", File name: " + file.name + ", File size: " + file.size + ", Message: " + message);
			break;
		}
	} catch (ex) {
        this.debug(ex);
    }
}

function uploadComplete(file) {
   
	if (this.getStats().files_queued === 0) {
		//document.getElementById(this.customSettings.cancelButtonId).disabled = true;
	}
  
   var status = document.getElementById("divStatus");
   var existeValor = document.getElementById("contenido");
   if(existeValor != null){
        // limpiarTodoDiv();
   }
   var separator =(file.name).split(".");
   var extension = separator[1];
    extension = extension.toUpperCase();
    
    var upload_path = this.settings.upload_path;
    
    
    if (( extension == 'JPG') || (extension == 'JPEG')){
   		//Creacion de tag de imagenes
	   var elem = document.createElement("img");
	   elem.setAttribute("id", "contenido");
	   elem.setAttribute("src", upload_path + file.name);
	   elem.setAttribute("height", "120");
	   elem.setAttribute("width", "120");
	   elem.setAttribute("alt", "Flower");
	   $("#divStatus").html(elem);
   
    }
    else {
        if ( extension == 'MP4'){
      		//Crear tag de videos de mp4
            var mv = document.createElement("video");
            var nombre = upload_path + file.name; 
            mv.setAttribute("id", "contenido");
            mv.controls = true;
            mv.setAttribute("src", "uploads/" + file.name);
            mv.setAttribute("type", "video/mp4");
            $("#divStatus").html(mv);
        
        }else{
        
            if ( extension == 'MP3'){

	          	//crear tag de audio mp3.
	            var mv = document.createElement("audio");
	            var nombre = upload_path + file.name; 
	            mv.setAttribute("id", "contenido");
	            mv.controls = true;
	            mv.setAttribute("src", nombre);
	            mv.setAttribute("type", "audio/mp3");
	            $("#divStatus").html(mv);
	           
            }
        }

         document.getElementById("museo_backendbundle_multimediatype_idioma").style.display = 'block';
   }

   //incluyo el nombre del archivo en el input oculto
   document.getElementById("museo_backendbundle_multimediatype_fileName").value = file.name;
}

// This event comes from the Queue Plugin
function queueComplete(numFilesUploaded) {
	
 
  
   
}

 function limpiarDiv(id)
        {
         var div;
 
         div = document.getElementById("areaTexto");
 
          var d = document.getElementById('divStatus');
          var d_nested = document.getElementById('areaTexto');
          var throwawayNode = d.removeChild(d_nested);
          
         
        }
        
  function limpiarTodoDiv()
  {
   var tagEliminar;

   //tagEliminar = document.getElementById('contenido');
   var d = document.getElementById('divStatus');
   var d_nested = document.getElementById('contenido');

   var throwawayNode = d.removeChild(d_nested);
   //elemento.parentNode.removeChild(tagEliminar);
    
  }

        
    function rellenarDiv(id, texto)
        {
            var div;
            var nodoTexto;
 
              div = document.getElementById(id);
 
              nodoTexto = document.createTextNode(texto);
 
              //Llamamos a la funci�n limpiarDiv para eliminar el contenido
              //actual del div.
              limpiarDiv(id);
 
              div.appendChild(nodoTexto);
        }

    